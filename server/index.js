const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const User = require('./schemas/user');
const mongoose = require('mongoose');
const session = require('express-session');

app.use(session({
    secret: 'keyboard key',
    resave: false
}));

mongoose.Promise = Promise;
mongoose.connect('mongodb://localhost:27017/angulardb')
    .then(() => console.log('mongoose run '));


app.use(bodyParser.json());

app.post('/api/login', async (req, res) => {
    const {email, password} = req.body;
    console.log(email, password);
    const resp = await User.findOne({email, password});
    console.log(resp, '@@@@@@@@@@@@@');
    if (!resp) {
        console.log("incorrect log");
        res.json({
            success: false,
            message: "Incorrect details"
        });
    } else {
        res.json({
            success: true
        });
        req.session.user = email;
        req.session.save();
        console.log("correct log");
    }
});


app.post('/api/register', async (req, res) => {
    const {name, email, password} = req.body;
    const existingUser = await User.findOne({name});
    if (existingUser) {
        res.json({
            success: false,
            message: "Email already used"
        });
    }
    const user = new User({
        name: name,
        email: email,
        password: password

    });
    const result = await user.save();
    res.json(res.json({
        success: true,
        message: "ok"
    }));
});

app.get('/api/isloggedin', (req, res) => {
    console.log('%%%%%%%%%%%%%%',req);
    res.json({
        status: !!req.session.user
    })
});

app.get('/api/data', async (req, res) => {

    console.log('***********',req.session);
    const user = await User.findOne({email: req.session.user});
    if (!user) {
        res.json({
            status: false,
            message: 'user unavailuble'
        });
        return
    }
    res.json({
        status: true,
        email: req.session.user,
        name: user.name
    })
});


app.listen(1234, () => {
    console.log('server listen 1234')
});
