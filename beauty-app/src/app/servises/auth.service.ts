import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedInStatus = JSON.parse(localStorage.getItem('loggedIn')) || false;

  constructor(private http: HttpClient) {

  }

  setLogInStatus(status) {
    this.loggedInStatus = status;
    localStorage.setItem('loggedIn', status);
  }

  /*  get isLoggedIn() {
      return JSON.parse(localStorage.getItem('loggedIn')) || this.loggedInStatus;
    }*/


  registerUser(data) {
    return this.http.post('/api/register', {
      name: data.name,
      email: data.email,
      password: data.password

    });
  }

  loginUser(data) {
    return this.http.post('/api/login', {
      email: data.email,
      password: data.password
    });
  }

  isLoggedIn() {
    return this.http.get('/api/isloggedin');
  }


  getData() {
    return this.http.get('/api/data');
  }

  logout() {
    return this.http.get('/api/logout');
  }

}
