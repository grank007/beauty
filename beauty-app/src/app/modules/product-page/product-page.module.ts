import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductPageComponent} from './product-page/product-page.component';
import {MatButtonModule, MatExpansionModule, MatTableModule, MatTabsModule} from '@angular/material';
import {RouterModule} from '@angular/router';


@NgModule({
  declarations: [ProductPageComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatExpansionModule,
    RouterModule,
    MatTabsModule,
    MatTableModule
  ],
  exports: [
    ProductPageComponent
  ]
})
export class ProductPageModule {
}
