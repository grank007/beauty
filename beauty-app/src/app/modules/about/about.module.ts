import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AboutComponent} from './about/about.component';
import {RouterModule} from '@angular/router';
import {MatButtonModule} from '@angular/material';


@NgModule({
  declarations: [AboutComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatButtonModule
  ],
  exports: [
    AboutComponent
  ]
})
export class AboutModule {
}
