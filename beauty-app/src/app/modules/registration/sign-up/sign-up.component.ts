import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../../servises/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  signUpForm = new FormGroup({
    name: new FormControl(''),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('')
  });

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  sendForm(event) {
    event.preventDefault();
    this.authService.registerUser(this.signUpForm.value).subscribe((resp: any) => {
      if (resp.success) {
        this.router.navigate(['/sign-in']);
      }
    });
  }


}
