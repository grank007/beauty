import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegistrationPageComponent} from './registration-page/registration-page.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {SignInComponent} from './sign-in/sign-in.component';
import {MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

const components = [
  RegistrationPageComponent,
  SignUpComponent,
  SignInComponent
];

@NgModule({
  declarations: [
    ...components

  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    HttpClientModule
  ],
  exports: [
    ...components
  ]
})
export class RegistrationModule {
}
