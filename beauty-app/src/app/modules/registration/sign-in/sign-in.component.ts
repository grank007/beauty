import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../../servises/auth.service';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  signInForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('')
  });

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  loginUser(event) {
    event.preventDefault();
    this.authService.loginUser(this.signInForm.value).subscribe((resp: any) => {
      if (resp.success) {
        debugger
        this.router.navigate(['/']);
        this.authService.setLogInStatus(resp.success);
      }
    });
  }

}
