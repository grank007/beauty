import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../servises/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  email;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.authService.getData().subscribe((data: any) => {
      if (data.status) {
        this.email = data.email;
      } else {
        this.router.navigate(['/sign-in']);
      }
    });
  }

}
