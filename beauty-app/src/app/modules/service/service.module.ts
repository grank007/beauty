import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ServiceComponent} from './service/service.component';
import {RouterModule} from '@angular/router';


@NgModule({
  declarations: [ServiceComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    ServiceComponent
  ]
})
export class ServiceModule {
}
