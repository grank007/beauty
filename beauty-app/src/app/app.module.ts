import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RegistrationModule} from './modules/registration/registration.module';
import {MatButtonModule, MatInputModule} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import {HomeModule} from './modules/home/home.module';
import {RouterModule} from '@angular/router';
import {SignInComponent} from './modules/registration/sign-in/sign-in.component';
import {SignUpComponent} from './modules/registration/sign-up/sign-up.component';
import {HomeComponent} from './modules/home/home/home.component';
import {AuthGuard} from './servises/auth.guard';
import {AboutModule} from './modules/about/about.module';
import {AboutComponent} from './modules/about/about/about.component';
import {ServiceModule} from './modules/service/service.module';
import {ProductPageModule} from './modules/product-page/product-page.module';
import {ProductPageComponent} from './modules/product-page/product-page/product-page.component';
import {UserPageModule} from './modules/user-page/user-page.module';
import {UserPageComponent} from './modules/user-page/user-page/user-page.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RegistrationModule,
    AboutModule,
    MatInputModule,
    MatButtonModule,
    HomeModule,
    ServiceModule,
    ProductPageModule,
    UserPageModule,
    RouterModule.forRoot([
      {
        path: 'sign-in',
        component: SignInComponent
      },
      {
        path: 'sign-up',
        component: SignUpComponent
      },
      {
        path: 'about:type',
        component: AboutComponent
      },
      {
        path: 'about',
        component: AboutComponent
      },
      {
        path: 'product:id',
        component: ProductPageComponent
      },

      {
        path: 'user:id',
        component: UserPageComponent
      },
      {
        path: 'user',
        component: UserPageComponent
      },
      {
        path: 'product',
        component: ProductPageComponent
      },
      {
        path: 'home',
        component: HomeComponent,
        redirectTo: '',
        canActivate: [AuthGuard]
      },
      {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '**',
        component: SignUpComponent
      }

    ])

  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
